This repository contains settings files for [GrimoireLab](https://chaoss.github.io/grimoirelab) demos
used for talks, conferences, etc.

# How to run them

You need to have ElasticSearch, MariaDB and KiBiter running as basic infrastructure. To do it,
there is a `docker-compose.yml` file in [docker](docker) folder. Access to that folder and:

```shell
$ docker-compose up -d
```

Once the basic infra is running, you need the main GrimoireLab Python packages installed. 

I recommend to set up a virtual environment in Python3:

```shell
$ python3 -m venv grimoirelab
$ source grimoirelab/bin/activate
(grimoirelab) $
```

And now, install the `sirmordred` GrimoireLab Python package:

```shell
(grimoirelab) $ pip install sirmordred
```

Basically each folder has `projects.json` and `mordred.cfg` file which are the settings require to run 
GrimoireLab for each project. To run it, it should be a matter of navigating to such folder and run:

```shell
(grimoirelab) ~/project $ mordred -c <mordred-configuration-file.cfg>
```

# License

[GPL v3](LICENSE)